def produc(number_list):
    """
      >>> produc(range(2, 5, 2))
      8
      >>> produc(range(4, 7, 2))
      24
      >>> produc(range(4, 5, 2))
      4
    """

    produc = 1
    number_ran = number_list
    for number in number_ran:
        produc *= number
    return produc

if __name__ == '__main__':
    import doctest
    doctest.testmod()
