from gasp import *
from random import *

class Player:
    pass

class Robots:
    pass

def place_player():
    global player, key
    player = Player()
    player.x = randint(5,63)
    player.y = randint(5,47)
    player.c = Circle((player.x, player.y), 5)
def move_player():
    global player, key
    key = update_when('key_pressed')
    if key == 'd' and player.x < 63:
        player.x += 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'c':
        if player.x < 63:
            player.x += 1
        if player.y > 0:
            player.y -= 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'w' and player.y < 47:
        player.y += 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'a' and player.x > 0:
        player.x -= 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'x' and player.y > 0:
        player.y -=1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'z':
        if player.x > 0:
            player.x -= 1
        if player.y > 0:
            player.y -= 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'q':
        if player.x > 0:
            player.x -= 1
        if player.y < 47:
            player.y += 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'e':
        if player.x < 63:
            player.x += 1
        if player.y < 47:
            player.y += 1
        move_to(player.c, (10 * player.x + 5, 10 * player.y + 5))
    elif key == 'r':
        remove_from_screen(player.c)
        safely_place_player()

def place_robots():
    global robot, numbots, robots
    robots = []
    while len(robots) < numbots:
        robot = Robots()
        robot.x = randint(5,63)
        robot.y = randint(5,47)
        if not collided(robot, robots):
            robot.c = Box((10 * robot.x + 5, 10 * robot.y + 5), 10, 10, filled=True)
            robots.append(robot)

def move_robots():
    global robots, player
    for robot in robots:
        if robot.x < player.x:
            robot.x = robot.x + 1
        elif robot.x < player.x and robot.y < player.y:
            robot.x += 1
            robot.y += 1
        elif robot.x < player.x and robot.y > player.y:
            robot.x += 1
            robot.y -= 1
        elif robot.x > player.x and robot.y > player.y:
            robot.x -= 1
            robot.y -= 1
        elif robot.x > player.x and robot.y < player.y:
            robot.x -= 1
            robot.y += 1
        elif robot.x > player.x:
            robot.x = robot.x - 1
        elif robot.y < player.y:
            robot.y = robot.y + 1
        elif robot.y > player.y:
            robot.y = robot.y - 1
        move_to(robot.c, (10 * robot.x + 5, 10 * robot.y + 5))

junk = []

def check_collisions():
    global player, robots, finished, junk
    surviving_robots = []
    for robot in robots:
        if collided(robot, junk):
            continue
        if collided(player, robots+junk) == True:
            print("You've been caught!")
            sleep(1)
            finished = True
            return
        jbot = robot_crashed(robot)
        if jbot == False:
            surviving_robots.append(robot)

        else:
            remove_from_screen(jbot.c)
            jbot.shape = Box((10*jbot.x, 10*jbot.y), 10, 10)
            junk.append(jbot)
    robots = []
    for living in surviving_robots:
        if collided(living, junk) == False:
            robots.append(living)
    if robots == False:
        finished = True
        Text("You Won!", (100, 200), size=30)
        sleep(3)
        return
      

def robot_crashed(the_bot):
    for a_bot in robots:
        if a_bot == the_bot:
            return False
        if a_bot.x == the_bot.x and a_bot.y == the_bot.y:
            return a_bot
    return False

def safely_place_player():
    global robots, player 
    place_player()
    while collided(player, robots) == True:
        place_player()

def collided(player, robots):
    for robot in robots:
        if player.x == robot.x and player.y == robot.y:
            return True
    return False

begin_graphics()
numbots = int(input("How many robots do you want? " ))
place_robots()
safely_place_player()
finished = False
while finished == False:
    move_player()
    move_robots()
    check_collisions()
end_graphics()
