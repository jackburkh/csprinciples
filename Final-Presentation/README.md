# Turtles
![twertl.jpeg](twertl.jpeg)
## Basic's for Turtle
Turtles can move forward, backward, and turn left or right.   
![twertle2.jpeg](twertle2.jpeg)

## Art :)
![square.svg](square.svg)
```
from svg_turtle import SvgTurtle
colors=["blue","green","yellow","pink"]
gus = SvgTurtle()
for x in range (100):
    gus.pencolor("blue")
    gus.width(2)
    gus.forward(x)
    gus.left(91)
gus.save_as('square') 
```
This is a very simple shape to make.
You start by setting the pen color to blue. Then, make a loop that draws a line and turns left 91 degrees. This repeats 100 times and you are given this shape. 
![svg.svg](svg.svg)
```
from svg_turtle import SvgTurtle

colors=["red","yellow","black"]

gus = SvgTurtle

for x in range(2000):
    gus.pencolor(colors[x%3])
    gus.width(2)
    gus.forward(x*2)
    gus.left(139)

gus.save_as('svg')
```
To draw this shape, first I made a list of the colors that I wanted to use. Then I made a loop that used the modulo operator to select a color. The code then draws a line and rotates left 139 degrees. The modulo operator makes the code rotate colors every time the loop runs.  
![pinwheel.svg](pinwheel.svg)
To draw this I used a for loop inside another for loop, which allows me to change color, and turn without increasing the distance that the turtle travels. 
```
from svg_turtle import SvgTurtle

colors=["blue","green","purple"]

gus = SvgTurtle()

for x in range(2000):
    for y in range(7):
        gus.pencolor(colors[x%3])
        gus.width(5)
        gus.forward(x*2)
        gus.left(151)
gus.save_as('pinwheel')
```
![hex.svg](hex4.svg)
```
from svg_turtle import SvgTurtle
colors = ["red", "cyan", "green", "yellow", "black", "orange"]
gus = SvgTurtle()

for x in range(200):
    gus.pencolor(colors[x%6])
    gus.width(x/100+1)
    gus.forward(x/2)
    gus.left(59)

gus.hideturtle()
gus.save_as('hex3')                   
```
This shape uses the same principle as the fisrt shape. First, it selects a color, goes forward, and turns left. Then it repeats. 
![sun.svg](start.svg)
```
from svg_turtle import SvgTurtle
 
 
tur = SvgTurtle(750,500)
tur.speed(20)
tur.color("black", "orange")
tur.begin_fill()

for i in range(36):
    tur.forward(300)
    tur.left(170)


tur.end_fill()
tur.save_as('start.svg')
```
This was made using a for loop and turning 10 degrees each time and turning a total of 36 times. This means the turtle goes in a full circle and draws a star. Then I filled in the star.

![blueangels.svg](blueangels.svg)

```
from svg_turtle import SvgTurtle
from random import *
r=0
t = SvgTurtle(500, 500)
t.pensize(1)
t.speed(0)
for n in range(0, 5000):
    randint1 = randint(0,360)
    randint3 = randint(400,600)
    t.seth(int(randint1))
    if r % 4 == 0:
        t.pencolor("blue")
    elif r % 4 == 1:
        t.pencolor("green")
    elif r % 4 == 2:
        t.pencolor("cyan")
    elif r % 4 == 3:
        t.pencolor("lime green")
    t.circle(randint3, 359.9)
    r+=1
t.save_as("blueangels.svg")
```

This image uses a for loop looped 5000 times that creates different colored circles that go off-screen. This causes around a third of the circle to be shown which creates the curving effect.


## How to switch to SVG file
Changing a regular turtle code to an SVG file is simple but  requires a few steps. 
1. Change the first line from ```from turtle import *``` to ```from svg_turtle import SvgTurtle```
2. Instead of saying ```gus = turtle``` you have to change it to ```gus = SvgTurtle```
3. Make sure that before every turtle command you add in your turtles name and then a period. For example ```gus._____```
4. At the end, include your turtles name before .save_as('example.svg') and then inside of the quotes type in the name you would like the file to save as. For example ```gus.save_as('art.svg')```
5. When using colors make sure that you use a color that is accepted by html as it is also accepted by svgturtle. rgb does not work.