## Rockwell's Group

- Make sure you know all the instructions.
- Most important thing is to study and practice as much as you can without cramming.
- Big Idea 3 has the most questions out of all Big Ideas.
- Khan Academy can be used to study.
- Questions on the AP test are in pseudocode.
- Exam has a 66% pass rate.
- College board website.

## Practice Problems

[Source](www.khanacademy.org/computing/ap-computer-science-principles)

### Question 1

Which of these values can be stored in a single bit?

- [x] 0
- [ ] [0, 1]
- [ ] [2, 4]
- [ ] 2

### Question 2

Which of the following is a single byte of digital information?

- [ ] 0
- [x] 10101101
- [ ] 101011010111
- [ ] 10
- [ ] 1101010

### Question 3

Consider this sequence of bits:

1011001010010010

How many bytes long is that sequence of bits?

- [x] 2
- [ ] 8
- [ ] 16
- [ ] 4
