# Computing Systems and Networks Vocabulary

- Bandwidth
> The maximum amount of data that can transfer over connections.
- Computing Device
> A piece of technology that can carry out well sized computations.
- Computing Network
> A system of connected computing devices that can transfer data between one another.
- Computing System
> A functional piece of hardware and software that can be used together.
- Data Stream
> Data being constantly created by many sources.
- Distributed Computing System
> Multiple computers working together to carry out a function.
- Fault-Tolerant
> How well a computing system can continue to carry out its function when one or more of its components stop working.
- Hypertext Transfer Protocol (HTTP)
> A protocol used to send and recieve data, files and webpages on the internet.
- Hypertext Transfer Protocol Secure (HTTPS)
> An extended version of HTTP that encrypts data when used, hence the added "S" added to the end of the name to make it "Secure"
- Internet Protocol (IP) Address
> Internet Protocol indicated where packets should be sent.
- Packets
> A small piece of a large sample of data.
- Parallel Computing System
> A way for multiple computers to work together at the same time by dividing up the work.
- Protocols
> A set of rules that tells data how to be transmitted between computing devices.
- Redundancy
> A process of giving data multiple paths to travel in incase one of them fails.
- Router
> A device that connects multiple networks.
