# TinyML Notes

## Edge Computing

- Collect and process data at the source
- Greater and higher quality data collection
- Less central processing
- Less network traffic & thus less latency
- Data privacy / security
- Specialized hardware
- Redundancy and scalability

## Raspberry Pi and Esp32

- Both examples of edge computing
- Esp32 does not have its own OS but Raspberry Pi does.
- Esp32 uses embedded coding which increases the efficiency of data processing. But, Esp32 has a less powerful processor than a Raspberry Pi.

## Machine Learning

- You could feed a lot of pictures of birds to a Machine Learning AI, you could train it to recognize birds in a picture, at the end of the day, it is just analyzing each pixel and bit of data in the image and finding patterns.
- ChatGPT is an example of a neural network
- A neural network is a way for Machine Learning to process data like the human brain.
- Machine Learning is the ability for a computer to program itself.



## Other Notes

- 8 dollars is actually a pretty good price for an Esp32 Cam.
- Fuzzy logic is an outdated term.
