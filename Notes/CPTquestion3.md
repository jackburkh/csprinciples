# Written Response (Question 3) for Create Performance Task

## 3a. Provide a written response that does all three of the following:

1. Describes the overall purpose of the program
> The purpose of the program is to create a game where the player runs from robots.
2. Describes what functionality of the program is demonstrated in the
   video.
> When the player moves, the robots will move towards the player. If a player runs into junk or a robot the game ends, if two robots collide, they become junk.
3. Describes the input and output of the program demonstrated in the
   video.
> The input is a key being pressed, the output is the player moving, and then the robot moving towards the player.

## 3b. Provide a written response to the following:
 
Capture and paste two program code segments you developed during the
administration of this task that contain a list (or other collection
type) being used to manage complexity in your program.

1. The first program code segment must show how data have been
   stored in the list.
>  ![list1](list1.png)
> This list makes a list named "robots". It is filled with the robot objects.
2. The second program code segment must show the data in the same list being
   used, such as creating new data from the existing data or accessing
   multiple elements in the list, as part of fulfilling the program’s purpose.
> ![list2](list2.png)
> This code goes through each robot object and moves it towards the player.


## Then provide a written response that does all three of the following: 

1. Identifies the name of the list being used in this response
> The name of the list in this response is named "robots".
2. Describes what the data contained in the list represent in your
   program
> The data that is contained in the list represents all living robots.
3. Explains how the selected list manages complexity in your program code by
   explaining why your program code could not be written, or how it would be
   written differently, if you did not use the list
> If this list wasn't in the code, it would be much harder to change the number of robots in the game.

## 3c. Provide a written response to the following:

Capture and paste two program code segments you developed during the
administration of this task that contain a student-developed procedure that
implements an algorithm used in your program and a call to that procedure.

1. The first program code segment must be a student-developed
   procedure that
   - Defines the procedure’s name and return type (if necessary)
   - Contains and uses one or more parameters that have an effect on the
     functionality of the procedure
   - Implements an algorithm that includes sequencing, selection, and
     iteration
> ![collided1](collided1.png)
> This procedure takes two inputs, a list, and an item. It checks if the item is in the same coordinate as anything in the list. It loops through the list to check if the item is in the same coordinate as anything in the list, it will return either true or false.
2. The second program code segment must show where your student-developed
   procedure is being called in your program.
> ![collided2](collided2.png)
> The procedure is used to check if any robots are in the same coordinate as the player.

## Then provide a written response that does both of the following:

3. Describes in general what the identified procedure does and how it
   contributes to the overall functionality of the program
> The procedure checks if anything in the list is in the same coordinate as the item, it wil end the game if the player is in the same coordinate as a robot or junk, and will also kill robots that are in the same coordinate as another robot or junk.
4. Explains in detailed steps how the algorithm implemented in the identified
   procedure works. Your explanation must be detailed enough for someone else
   to recreate it.
> The procedure loops through the list. In each loop, it checks if the x and y coordinate of the item is the same as the x and y coordinate of the current item in the list.

## 3d. Provide a written response that does all three of the following:

1. Describes two calls to the procedure identified in written response 3c. Each
   call must pass a different argument(s) that causes a different segment of
   code in the algorithm to execute.

> The procedure identified in 3c is only called inside the loop so I will use a different procedure, which is the collided function. ![collided1](collided1.png)



> First call:
> The first call passes the arguments "player" for the item and "robots" for the list.

> Second call:
> The second call passes the arguments "robot" for the item and "junk" for the list.

2. Describes what condition(s) is being tested by each call to the procedure

> Condition(s) tested by the first call:
> The first call tests if the player is in the same spot as any of the robots.

> Condition(s) tested by the second call:
> The second call tests if a robot is in the same spot as any other dead robots.

3. Identifies the result of each call

> Result of the first call:
> If the player is in the same spot as any of the robots, then it returns true. Otherwise, it will return false.

> Result of the second call:
> If the selected robot is in the same spot as any of the junk, then it returns true. Otherwise, it will return false.
