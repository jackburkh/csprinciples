# Chapter 7 Notes

## Repeating Steps

* Use a **for** loop to repeat code.
* Use a **range** to create a list of numbers.
* Loops are helpful for doing something many times.
* Computers can do the same thing over and over as long as it doesn't lose electricity.
* When computers repeat a step in a program, it is called a loop or iteration.

## Repeating with Numbers

* A **for** loop is one type of loop or way to repeat a statement or set of statements in a program. 
* A **for** loop will use a variable and make the variable take on each of the values in a list of numbers one at a time.
* A **list** holds values in an order.
* The statements inside the for **loop** must be **indented** be one more than the **for loop**.
* A line that has a **for loop** on it needs to end with :.
* You always need at least one program in a body of a **loop**.
* If a line/statement is put in the indented part of a loop, they will run with the loop.

## What is a List?

* A **list** holds items in order.
* A **list** in Python is enclosed in [ and ] and can have values separated by commas.
* A **list** has an order and each list item has a position in the list.
* The computer doesn't care if we name the variable xyzzy1776. It will work with a bad variable name. It's just not as readable.
* You should write your programs so that people can understand them, not just computers.

## The Range Function

* You can use the **range** function to loop over a sequence of numbers.
* If the range function is called with a single positive integer, it will generate all the integer values from 0 to one less than the number it was passed and assign them one at a time to the loop variable.
* A range function is inclusive of the first value and exclusive of the second value.
* You can also use 3 positive integers with a range function, the first one being the starting number, the second being the end value, and the third being the difference in each value in between.

## There's a Pattern Here!

* A pattern that is common when processing data is called an Accumulator Pattern.
* The five steps for this pattern are:
    1. Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
    2. Get all the data to be processed.
    3. Step through all the data using a for loop so that the variable takes on each value in the data.
    4. Combine each piece of the data into the accumulator.
    5. Do something with the result.
* With a negative third integer in a range function, you can step downwards in the range function.


