# Big Idea #1 Vocabulary Definitions

* Code Segment
> A segment of code in a program.

* Collaboration
> To work together with one or more people to accomplish a task.

* Comments
> A segment of text that does not affect the program, but can be used to leave comments for different reasons, such as identifying what segments of code do.

* Debugging
> The process of finding errors and fixing them in a body of code.

* Event-driven Programming
> A type of programming that detects events when they occur and use specified procedures when these events occur.

* Incremental Development Process
> A type of programming that splits parts of the code up into fully working segments that will be pieced together to create the end product.

* Iterative Development Process
> A type of programming that adds features or procedures one after another.

* Logic Error
> A type of error in programming that causes the program to still operate but operate incorrectly.

* Overflow Error
> A type of error that occurs when the data type used to store information was not big enough to hold that much data.

* Program
> A body of text or code that is intended to carry out specific functions.

* Program Behavior
> A type of programming that causes the program to carry out independent procedures.

* Program Input
> What the user gives the program.

* Program Output
> What the program gives to the user.

* Prototype
> The first version of a creation that may not work completely correct.

* Requirements
> The needs or criteria needed to perform an action.

* Runtime Error
> A type of error in the code that occurs when the code is syntactically correct but has a different undetectable error.

* Syntax Error
> A type of error that occurs when a character is incorrectly placed in the code.

* Testing
> The action of using a creation to observe if it will work correctly, or to see what it will do.

* User Interface
> The point of where humans can interact with the computer. 
