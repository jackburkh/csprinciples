# Pair Programming

## What is Pair Programming?

* Pair programming is the practice of two or more people or groups collaborating with each other to create a program.
* Pair programming used to mainly be people programming on the same computer, with one person essentially building the program, and the other helping them with questions and such. Now, many people have begun to pair program online.
* Online or remote programming platforms have been increasing in popularity, with more and more programmers beginning to use them to pair program.

![pairprogramming](pairprogramming.png)

## Why Pair Program?

* Pair programming can be useful for a multitude of reasons:
  1. With two people working together, you are more likely to catch mistakes.
  2. Since there are two or more people, you can work faster.
  3. You can create a better program with multiple people working on it at once.
  4. It can be a useful resource to help less skilled programmers learn from people they are pair programming with.
  5. With programs like Replit, programmers can work on the same program at the same time.

## In-Person Pair Programming
### The Driver-Navigator system
* The driver-navigator system is a type of in-person pair programming where one person is the "driver" and the other person is the "navigator".
* The driver is the person who is actually controlling the computer, writing the code.
* The navigator is there to point out problems or something they could add to the program, they also answer questions that the driver has.

![pairprogramming2](pairprogramming1.png)

## Agile Pair Programming
### Agile is essentially a way for teams to program in an iterave approach.
* Agile is an iterave approach to programming, meaning the programmer(s) slowly add on small improvements/additions to the program.
* Teams work in small but easier to develop and test increments.
* It allows for teams to split up work and easily combine or remove pieces of the program.
* Allows for an early release of the program/software that teams are programming, as they can add more to it later in development, even after it is released.
  
