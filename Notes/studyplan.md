

- The Test is made of 70 questions, 57 being single-select multiple choice questions, 5 being single-select with reading passages, and 8 multiple-select multiple-choice questions.

# Score Makeup

- 30% of the score is made of the Create Performance Task, a task to make a computer program either cooperatively or individually, preferably cooperatively. Though, you need to do a reflection on the program and understand what it does. The Create Performance Task is scored by someone on the college board.

- You need a 3, 4, or a 5 on the AP exam to pass and get a valid college credit.

- The other 70% of the grade is by the actual tested part.

- In 2021, 67% of students passed the Computer Science AP class exam (3 or higher). It isn't exactly very clear on the required score to pass or get a 4 or a 5, but to pass it is most likely around 60-70% on the exam.

# Reflection

- I scored the best on the Big Idea 3, and the worst on the Big Idea 4, I need to improve on all of them, but mainly Big Idea 4.

- A very good place to study for the AP exam is Khan Academy, as well as code.org, and also obviously the course itself.

