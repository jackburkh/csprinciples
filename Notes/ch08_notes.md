# Loops - For and While

* The body of loops are lines that are indented that are the part of the loop that is repeating.

* For loops are loops used to repeat a body a known number of times.

* While loops are loops used to repeat a body an unknown number of times, and until something specified happens or while something is happening.

* A while loop will repeat the body of loop as long as a logical expression is true. An example of a logical expression is x > 3.

*  Nested loops are loops that are within each other, such as a for loop being inside a while loop.

* The logical expression in a while loop will be checked before every iteration of the loop.

* The way to see how many times a nested for loop will run is to multiply the amount of outside loops there will be by the inside loop.
