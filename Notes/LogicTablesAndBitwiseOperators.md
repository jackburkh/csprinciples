# Bitwise Operatos & Logic Tables

## Bitwise Operators

> Bitwise Operators are a way for us to change bits in certain ways using logic.

|Operator|Truth Table Equivalent|Explanation|
| :---: | :---: | :---: |
|^|XOR|Operator returns a 1 in a position where there is one 1 in either of the corresponding bits, but not a 1 in both of the corresponding bit positions. Ex. 1001 ^ 0001 = 1000|
|~|NOT|Operator acts like the AND operator, but inverts the value of each bit afterwards. Ex. 1010 ~ 1110 = 0101.
| \ |OR|Operator returns a 1 in position where there is atleast one 1 in either of the corresponding bits. Ex. 1010 \ 0001 = 1011|
|&|AND|Operator returns a 1 in position where there are 1s in both of the corresponding bit positions. Ex. 1010 & 1000 = 1000|
|>>|RIGHT SHIFT|Operator shifts all of the bits to the right by a certain amount specified by the second argument/value. Ex. 1010 >> 0001 = 0101|
|<<|LEFT SHIFT|Operator shifts all of the bits to the left by a certain amount specified by the second argument/value. Ex. 1010 << 0001 = 0100|


> All bitwise operators have a corresponding compound operator, such as the + operator, how it has a corresponding += operator. These bitwise operators can do the same thing, such as &=, which will be equivalent to a = a & b.

## Logic Tables

> Logic tables correspond with bitwise operators, bitwise operators are just ways to represent each logical function.

# &
|A|B|Output|
| :---: | :---: | :---: |
| 1 | 1 |1 |
| 1 | 0 |0 |
| 0 | 1 |0 |
| 0 | 0 |1 |

# |
|A|B|Output|
| :---: | :---: | :---: |
| 1 | 1 |1 |
| 1 | 0 |1 |
| 0 | 1 |1 |
| 0 | 0 |0 |

# ^
|A|B|Output|
| :---: | :---: | :---: |
| 1 | 1 |0 |
| 1 | 0 |1 |
| 0 | 1 |1 |
| 0 | 0 |0 |

# ~
|A|B|Output|
| :---: | :---: | :---: |
|1010|0101|1111 |
|1101|0110|1011 |
|0000|1111|1111 |
|1001|0100|1101 |

# <<
|A|B|Output|
| :---: | :---: | :---: |
|1010|0001|0100 |
|1101|0010|0100 |
|0001|0011|0100 |
|1001|0100|1000 |

# >>
|A|B|Output|
| :---: | :---: | :---: |
|1010|0001|0101 |
|1101|0010|0011 |
|1000|0011|0010 |
|1001|0100|0001 |

