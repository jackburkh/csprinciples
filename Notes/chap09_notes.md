# Chapter 9 - Repeating Steps with Strings

## Using Repitition with Strings

* A string is a collection of letters, digits, and other characters.

* A for loop knows how to step through letters, and addition puts strings together.

* The 5 steps in the accumulator pattern are:

	1. Set the accumulator variable to its initial value. This is the value we want if there is no data to be processed.
	2. Get all the data to be processed.
	3. Step through all the data using a for loop so that the variable takes on each value in the data.
	4. Combine each piece of the data into the accumulator.
	5. Do something with the result.

## Reversing Text

* To reverse text in for loops using strings, you swap the variable and the thing you are adding to it to accumulate it backwards. (Ex. If you would normally have the for loop as a = a + b, you would rewrite it as a = b + a)

* The reason this happens is because you are adding each new letter at the beginning of the string.

## Mirroring Text

* You can also use this concept to mirror text too.

* To do this, add the letter to both sides of the string.

* If you would like to have something that isn't mirrored in the middle of the string, just whatever it is you want to add to the string before accumulating.

* You can use mirroring text as well as reversing text to create a palindrome.

## Modifying Text

* Using a while loop can modify specified text an uncertain amount of times, such as replacing all 1's with i's

* You can also replace entire words or phrases, or go even bigger if you wanted, this could help with auto-correction for mispelling.

* You can also have different strings' characters be in the same position for that string and make encoded messages.

