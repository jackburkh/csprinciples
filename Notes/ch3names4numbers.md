# ***Chapter 3: Names for Numbers Notes***

## Assigning a name

- A computer can associate a name with a value. The way it does this is by creati
ng a variable. The variable is space in computer memory that can represent a val
ue. (ex. a score in a video game).

- A very basic example assignment of a variable would be a=4.

- Variable names must start with a letter or an underscore.

- Variable can't be keywords in *Python* such as **if, else, elif, etc.**

- Variable names also mustn't have spaces in them.

- *Assignment Dyslexia* is when you reverse the assignment statement (ex. 5=x), this will make it an illegal statement.

![image](~/Downloads/pongScore.jpg)

## How Expressions Are Evaluated

- The order of operations in math apply to programming as well, they follow the same rules and orders.

- "*" (Sign for multiplication), / (sign for division), % (sign for modulo), + (sign for addition), - (sign for subtraction).

- You can change the default order with parenthases like you can in math.
 

## Expressions

- The right side of the variable or what is being assigned doesn't have to be a value, it can be arithmetic and geometric, such as a=2+2 or a=2*2, they will be stored as 4 in the variable as that is what 2+2 or 2*2 is equal to.

- Fractions in a variable will be set to the decimal value when being stored and outputted.

- ***Modulo***, represented by the operator %, is like division (/), but instead of telling you the divided result, it tells you the remainder of the division equation.

-  An example of this would be 3 % 2, and since the remainder of 3/2 is 1, the outputted value will be one. 

## Walking Through Assignment More Generally

- The sequence of statements or assignments are very important. The variable 'x' might be equal to 12 at one point, and equal to 15 at another.

-  An assignment statement is something that happens once, and then is done with.

## More Info

- We can use variables for everyday things such as a spreadsheet with prices of items you bought.

- You could assign values to variables based off how much you spent and how many of them you bought, then tell the computer what to compute with arithmetic and it will output your answer.

- Variables can be *reused* after you are done using them.

# **Summary On Chapter 3**

- *Arithmetic Expression* - An arithmetic expression contains a mathematical operator like - for subtraction or * for multiplication.

- *Assignment* - Assignment means setting a variable’s value. For example, x = 5 assigns the value of 5 to a variable called x.

- *Assignment Dyslexia* - Assignment Dyslexia is putting the variable name on the right and the value on the left as in 5 = x. This is not a legal statement.

- *Integer Division* - Integer division is when you divide one integer by another. In some languages this will only give you an integer result, but in Python 3 it returns a decimal value, unless you use the floor division operator, //.

- *Modulo* - The modulo, or remainder operator, %, returns the remainder after you divide one value by another. For example the remainder of 3 % 2 is 1 since 2 goes into 3 one time with a remainder of 1.

- *Tracing* - Tracing a program means keeping track of the variables in the program and how their values change as the statements are executed. We used a Code Lens tool to do this in this chapter.

- *Variable* - A variable is a name associated with computer memory that can hold a value and that value can change or vary. One example of a variable is the score in a game.
