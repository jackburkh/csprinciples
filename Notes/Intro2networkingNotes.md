# Introduction To Networking Notes

### Introduction

## Chapter 1

- The technologies that made the internet today started to be created in the 1960s.
- The internet is much more complex that what it actually seems.

### 1.1

- You could use wires to communicate to a small amount of people at once but it becomes much harder to do it with many more people.
- An operator would have to plug in the wire of the person you are talking to.
- To make this an easier task, companies that handle our telephone communication have lots of central offices that connect us together.
- The longer the wire the more expensive it will be.
- Only one person could be using a long cable at a time, that's why long distance calls are priced and charged by the minute.

### 1.2

- Computers send messages to one another to check if the other is available.
- Computers used to be connected via wire.
- Messages would be sent through these wires for the computers to communicate.
- People could lease wire from companies for their computers.

### 1.3

- In the 1970s they found out that if a computer was connected to another that was connected to another the two end computers could send long distance messages.
- Though, this method would take lots of time to happen.

### 1.4
- Another way to send messages is to send them in small packs of data called packets.
- If a small packet was behind a large packet in the queue of sending them, it would have to wait for the entirety of the large packet to be sent before sending itself.
- Computers used to exist that were just meant to send and receive packets.
- Those computers are now known as routers but used be called IMPs.

### 1.5

- In the early ages of store and forward networks, you would need to know the "name" of the computer that the data was being sent to.
- This is known as the IP (internet protocol).
- When a packet was sent it needed to know the IP of the source and the IP of the destination, sort of like a letter.
- The packets needed a set of instructions to know how to be put back together and decompressed.

### 1.6

- Routers send packets through a path for them to get to their destination.
- Packets can now reach their destination before ones that were send before them.
- If a link on a path to the destination for the packets was broken, the router can find a new one to send them through.
- A router can recieve and send data and information through many computers.
- The term internet is derived from the idea of interworking.

## Chapter 2

### 2.1

- The link layer's purpose is to connect your computer to its local netwrok and transferring data across a single "hop".
- When you use a wireless device, the device will only communicate over a limited distance.
- The distance of wired connections is based off how long the wire is, but eventually the wire becomes ineffective if it is too long.
- Engineers designed a method called CSMA, if your computer wants to send data, it first listens if another computer is alreadyu sending data on the network. If there aren't any other computers sending data, your computer starts sending data. Your computer also sees if it can receive its own data during this time.

### 2.2

- Once your packet makes it to the first link, it will be located in a router.
- Each router handles the destinations of packets being sent.
- Routers react to network outages and work around them.

### 2.3

- The internetwork layer looks at a packet's desination and finds a path across multiple networks to deliver the packet.
- Each packet shows where it came from, where it's going and where the packet fits in data being sent.
- The sending computer of packets must store a copy of the parts of the original message that has been sent until the desination computer confirms that it has the data.

### 2.4

- The link, internetwork and transport layers work together to transport data quickly between two computers and across networks.
- Each application is broken into two halves, one is called the server, it runs on the destination computer and waits for incoming network connections. The other half is called the client and runs on the source computer. Ex. when you are running a web client such as Chrome, you are running a web client.

### 2.5

- The four last sections of notes are each different layers of communication (The link layer, the internetwork layer, the transport layer, and the application layer) all work together to transfer data between computers.
- All four layers run in your computer where you run the client, and all of them also run in the destination computer.

## Chapter 3

### 3.1

- When your phone is using WiFi to connect to the Internet, it is sending and receiving data with a small, low-powered radio.

- We call the first router that handles your computer's packet a gateway or base station.

- All computers that are close enough to the base station with their radios turned on receive all of the packets the base station transmits, regardless of where the packet's destination is.

- A rogue computer can abuse this and capture packets being sent like bank account details etc.

- Every WiFi radio in every device that is built is given a unique serial number in the form of a MAC address. It is usually shown in the form of 0f:2a:b3:1f:b3:1a. 

- The MAC address is like a "from" or "to" address on a postcard. Every packet sent across the WiFi has a source and destiation.

- Your computer needs to find out which of the MAC addresses on the WiFi network can send packets to the router. To do this, your computer sends a special message to a broadcasted address, pretty much asking who is in charge of this WiFi. Since your computer knows it isn't the gateway, it will send its MAC address along with this.

### 3.2

- Since many computers share the same radio frequencies it is important to coordinate how they send data.

- The first way this is done is called "Carrier Sense". It is to first listen for a transmission, then if there is already a transmission in progress, it will wait until the transmission finishes.

- If two computers start to transmit packets at the same time, it will corrupt and the packet will be lost.

- Your computer will listen to its own sent data to make sure it is the same thing as what it sent. If not, it will assume there is a collision in the data transmission.

### 3.3

- Sometimes when a link layer has many transmitting stations that need to operate at 100% efficiency for a long time, it will have a token that indicates when each station is given the opportunity to transmit data.

- When a station receives the token and has a packet to send, it will send the packet. 

- Once the packet has been sent, the station gives up the token and waits until the token comes back to it.

- The token is like a room of people passing around an object and only the person who has the object can speak.

## Chapter 4

### 4.1 

- The Internetowrking Protocol (otherwise known as IP) enlarges our network from a single hop link to multiple hops that make packets be efficiently routed from your computer to a destination and back to your computer.

- The IP layer is made to react an route around network outages, as well as maintaining ideal paths for routing packets between many computers.

### 4.2

- Each router learns its own place inside of the network its in, and cooperates with other routers to move packets efficiently.

- The IP layer can sometimes fail though, packets can be lost due to momentary outages or because the network fails to understand which path it needs to send a packet through.

### 4.3

- Packets that your system sends can later find a quicker route in the netowrk and arrive before packets that it sent earlier.

### 4.4

- The IP layer cannot be designed to make it so it doesn't lose any packets as it would make it very hard for the IP layer to handle very complex packets and data in so many systems.

### 4.5

- We leave packet loss up to the transport layer to be fixed because it would be very hard to fix packet loss within the IP layer.

### 4.6

- IP addresses have prefixes and many of them start with 192.168.

- Addresses with this prefix are called "non-routable" addresses.

- This means they will never be used to actually transport data across the network.

- But how does our computers connect to the global network and internet? Well, it does something called NAT, standing for Network Address Translation.

- NAT is a way of the computer sending packets, but the gateway will replace the address that the packets have with a routable one.

## Chapter 5

### 5.1

- The DNS (Domain Name System) is a system that allows end users to use symbolic names for servers, opposed to using numeric IP addresses.

- Domain names are assigned to an organization, such as edu, which is assigned to Educause. 

- These organizations can assign lower level sub-domains to other organizations. Ex. Educause could give the university of Michigan a subdomain called umich, so the subdomain and domain together would be umich.edu

- Domains ending in .com or .org can be purchased by people.

### 5.2 

- We read IP addresses from left to right in a way, with the prefix (the least specific part of the IP address) to the right, which is the most specific.

- This goes with domain names as well, except you read it from right to left, the right being the least specific, and the left being the most specific.

## Chapter 6

### 6.1

- When a packet travels from source to destination, it contains a link header, an IP header, and a TCP header, along with the actual data in the packet.

- The link header is removed when the packet is received on one link and a new link header is added when the packet is sent out on the next link.

- The IP and TCP headers stay with a packet as it is going across each link in its journey.

- The TCP headers indicate where the data in each packet belongs.

### 6.2 

- As the destination computer receives the packets, it looks at the offset position from the beginning of the message so it can put the packet into the proper place in the reassembled message.

- The Transport layer handles packets that arrive out of order by placing the packet data at the correct position relative to the beginning of the message.

- If a packet is lost, it will never arrive at the destination computer and so the destination computer will never send an acknowledgement for that data.

- The amount of data that the sending computer will send before pausing to wait for an acknowledgment is called the "window size".

### 6.3

- If a packet is lost, the destination computer keeps track of the amount of time since it received the last packet of data.

- At some point, the receiving computer decides too much time has passed and sends a packet to the sending computer indicating where in the stream the receiving computer has last received data.

- The sending computer backs up and resends data from the last position that the receiving computer had successfully received.

### 6.4

- The sending computer must hold on to all of the data it is sending until the data has been acknowledged by the destination computer.

- Once the receiving computer acknowledges the data, the sending computer can discard the sent data.

- The Transport layer continuously monitors how quickly it receives acknowledgements and dynamically adjusts its window size.

- This ensures that data is sent rapidly when the connection between two computers is fast and much more slowly when the connection has slow links or a heavy load.

### 6.5

- Packet headers contain information such as the source and destination IP addresses, the TTL for the packet, and the offset of each packet in the message or file being transmitted.

- The Transport layer handles packet reassembly, retransmission of lost packets, and dynamically adjusts the window size based on network conditions to optimize the speed of data transfer.

## Chapter 7

### 7.1

- Two parts are required for a networked application to function, the client and the server.

- The architecture for a networked application involves a client application that initiates requests and a server application that responds to those requests.

- The server provides a service or resource that the client can access.

- The client is usually a web browser, and the server is a web server that provides resources such as web pages, images, and other files.

### 7.2

- HTTPS stands for Hypertext Transfer Protocol Secure.

- A set of rules on how things work or run is called a protocol.

- HTTPS is a type of protocol.

- HTTPS is decently simple which is partially what makes it popular.

### 7.3

- Internet was made in 1985.

- 200 Status code means everything did good.

- 404 Status code means the document or website was not found.

- 301 Status code means the document or website was moved.

- The status codes starting with 2 means good, 3 means moved and 4 means something went wrong. Finally, 5, it means the server performed improperly.

### 7.4

- HTTP is one of the main server and client protocols used in the Internet.

- IMAP is a more complex protocol that has to be secure.

### 7.5

- The transport layer waits for acknowledgement.

- The applications are not responsible for the control of the flow.

### 7.6

- Sending data is very easy to do.

- Because of all the layers working together perfectly it makes sending and receiving data very easy

- In Python it is very easy to connect a web server.

### 7.7

- The lower layers make the application make the job easier for the higher levels and the higher levels fix problems with the lower layers.

- Because of this we have a large range of network applications.

## Chapter 8 

### 8.1

- Cyphers are encrypted messages with keys used to decrypt them.

- In Ancient Rome, the Ceaser Cypher was used, which was very hard to decrypt at the time.

- All encryptions have some key to solving it.

### 8.2

- People used to call or talk to each other and tell each other the key to decrypt encryptions.

- In the 1970s, people came up with the idea of an asymmetric key is a type of key where one key encrypts the data and one key decrypts the data.

### 8.3

- Networking engineers didn't want to break the internet protocols that already existed so they added a smaller layer in between the transport and application layers.

- The application layer is able to request the transport layer to send data encrypted or non encrypted.

### 8.4

- Web browsers either use HTTP or HTTPS to use encrypted or non encrypted data. HTTPS is encrypted.

- HTTPS used to be expensive but is now much cheaper and is used very often.

- The lock on your browser on the left of the search bar indicates whether the website is encrypted or not.

### 8.5

- A rogue computer can send you public keys and use that to take your data because when you send back your encrypted data they have the key to decrypt it.

- To make sure you are on a trusted and secure site they will send you a public key signed by the Certificate Authority.

- Your browser automatically knows a lot of trusted Certificate Authorities when you download it.

### 8.6

- Secure connections are called Secure Sockets Layer or the Transport Layer Security.

- By having security in the transport layer it allows for the other layers to stay how they are.

