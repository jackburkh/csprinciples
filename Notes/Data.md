- abstraction
> Making a part of a code as simple as it can be while still functioning perfectly.
- analog data
> Continuous data that represents a range of values.
- bias
> A liking towards one thing rather than another.
- binary number system
> A base 2 number system that uses 1s and 0s.
- bit
> The smallest unit of digital data, usually shown as a 0 or 1.
- byte
> A unit of digital data that is made of 8 bits.
- classifying data
> The process of sorting data into sets based on patterns.
- cleaning data
> The process of removing or correcting incorrect, incomplete, or duplicate data.
- digital data
> Data that is represented by discrete values rather.
- filtering data
> The process of removing unwanted data.
- information
> Data that has processed and displayed in an understandable form.
- lossless data compression
> A way of compressing data that when uncompressed no data is lossed.
- lossy data compression
> A way of compressing data that loses or sacrafices some data.
- metadata
> A set of data that provides other information on another set of data or information.
- overflow error
> When a data type does not have enough storage to hold its data.
- patterns in data
> Patterns that show up continuously in data that can be used to identify trends.
- round-off or rounding error
> An error that occurs when a calculation produces a result that is slightly different from what was intended due to a limitation in precise
- scalability
> How well a program or system can be removed or added upon.
