def ootA(nL):
    sum = 0
    for num in nL:
        sum = sum + num
    return sum / len(nL)

nL = [90, 80, 75, 90, 83]
print(ootA(nL))

