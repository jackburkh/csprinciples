def calculateProduct(lastNum):
    total = 1
    numList = range(2, lastNum + 1, 2)
    for num in numList:
        total = total * num
    return total
print(calculateProduct(8))
