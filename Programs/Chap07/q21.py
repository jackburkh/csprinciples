def who_knows_why(a):
    sumrange = range(0, a, 2)
    productrange = range(1, a ,2)
    sum = 0
    product = 1
    for n in sumrange:
        sum += n
    for n in productrange:
        product *= n
    difference = product - sum
    average = (product + sum)/2
    differenceAverage = difference/average
    return differenceAverage
    
print(who_knows_why(10))
