def oot(stop):
    evens = range(0, stop + 1, 2)
    odds = range( 1, stop + 1, 2)
    product = 1
    sum = 0
    for n in evens:
        product = product * n
    for n in odds:
        sum = sum + n
    difference = product - sum
    average = (product + sum) / 2
    return difference/ average
print(oot(10))

