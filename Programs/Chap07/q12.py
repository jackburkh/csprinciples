def epic(n):
    product = 1
    numbers = range(1, n+1)
    for number in numbers:
        product = product * number
    return product
print(epic(5))

