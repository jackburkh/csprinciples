def sumOdd(lastNumber):
        sum = 0
        numList = range(1,lastNumber+1,2)
        for num in numList:
            sum = sum + num
        return sum

print(sumOdd(13))

