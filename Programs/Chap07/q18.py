def aFunc(start, stop):
    sum = 0
    product = 1
    numbers = range(start, stop + 1)
    for number in numbers:
        sum = sum + number
        product = product * number
    average = (sum + product) / 2
    return average
print(aFunc(1, 10))
