def sumEvens(lastNum):
    sum = 0
    numbers = range(0,lastNum+1,2)
    for number in numbers:
        sum = sum + number
    return(sum)

print(sumEvens(20))

