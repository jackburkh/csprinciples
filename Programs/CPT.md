# Communicating information visually helps get the message across

![input](CPTinput.png)

With this picture you can see the input that we are giving the computer. When looking at the input it is hard to visualize and understand how much money you will actually make with interest over a 10 year period. 

![output](cptgraphpng.png)

However, with these graphs it becomes much easier to understand. We can use images and graphs to really help us understand all the information available to us. Such as providing structure in assessing performances, sales, and even deadlines. Without being able to show information visually it would be harder to show the general public data.

Some basic functions with the matplotlib module are: 

![functions](CPTfunctions.png)
