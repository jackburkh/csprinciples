result = 4 + -2 *3

# How Expressions are Evaluated

print(result)

# This will output -2 due to order of operations. (-2 will be multiplied by 3 before being added to 4)

result = (4 -2) * 3
print(result)

# This will now print 6 due to order of operations. (4 will be added to -2 before being multiplied by 3).


# Driving from Chicago to Dallas

# Multi-equation calculations can be done with python with variables and expressions. As an example, these expressions will print out the price for a trip to Chicago.

distance = 924.7
mpg = 35.5
gallons = distance / mpg
cost_per_gallon = 3.65
cost_trip = gallons * cost_per_gallon
print(cost_trip)

# When printing a string, quotes must  be used, when printing a variable, no quotes are needed.

# Now I will compute how long it will take for ketchup to ooze down a table.

dripMPH = .028
FPM = 5280
dripFPH = dripMPH * FPM
MPH = 60
dripFPM = dripFPH / MPH
print("Ketchup speed in feet per minute:")
print(dripFPM)

# Going through variables more generally.

# Lines don't all execute at the same time, they go down the list one by one under regular conditions. Ex:

a = 1
b = 12.3
c = "Fred"

# These variables would all be assigned one by one.

# Variables can also be assigned to another variables. Ex:

a = 1
b = 12.3
d = a

# Printing values is a great way to understand the code and know what is going on. Ex:

print(d)

# Variables can be used to solve problems we solve in a spreadsheet, such as an invoice. Ex:

quantity1=3
unit_price1=2
total1=quantity1 * unit_price1
quantity2=1
unit_price=2
total2=quantity2 * unit_price2
grandtotal=total1 + total2
print(grandtotal)

# This will print the grand total of all items purchased.

# There are also 2 last things I would like to go over, the modulo operator and the two variations of division. Firstly, modulo, divides 2 numbers with %, but only outputs the remainder of the 2 numbers. Secondly, the two variations of division are float division and integer division. Float division gives a decimal answer while the integer division gives whole numbers.
