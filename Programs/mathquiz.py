from random import randint

correct = 0

for x in range(10):
    num1 = randint(1,10)
    num2 = randint(1,10)
    question = "What is " + str(num1) + " times " + str(num2) + "?"
    x = (int(input(question)))
    y = num1 * num2
    if  x == y:
        print("You got it!")
        correct += 1 
    else:
        print("Try Again!")

print("You got " + str(correct) + " questions correct!")

num3 = "Enter Number: "
a = (int(input(num3)))
if a == 100:
    print("W")
    correct += 1
else:
    print("L")
for x in range(3):
    win1 = randint(1, 10)
    win2 = randint(11, 20)
    win3 = randint(21, 30)

    winners = str(win1) + " " + str(win2) + " " + str(win3)

    print("And the three lucky winners are: " + winners + ".")

# Trying to make Jeff's code for a math quiz.

import operator
import random
import os

ops = {
        "+": (operator.add "plus"),
        "-": (operator.sub, "minus"),
        "*": (operator.mul, "times"),
        "//": (operator.floordiv, "divided by"),
        "%": (operator.mod, "modulo"),
}

correct = 0

os.system("clear")
print("Welcome to the Math Quiz!\n")

numqs = input("How many questions do you want in your quiz? ")
while True:
    try:
        numqs = int(numqs)
        assert numqs > 0
        break
    except:
        os.system("clear")
        numqs = input("Please enter a positive number: ")

os.system("clear")
op = input("What kind of questions do you want (enter +, -, *, //, or %)? ")
while not op in ["+", "-", "*", "//", "%"]:
    os.system("clear")
    op = input("Please enter +, -, *, //, or % :")
os.system("clear")

for q in range(numqs):
    num1 = random.randint(1, 10)
    num2 = random.randint(1, 10)
    answer = ops[op][0](num1, num2)
    question = f"What is {num1} {ops[op][1]} {num2}? "
    reponse = int(input(question))

    if reponse == answer:
        print("That's right - well done.")
        correct += 1
    else:
        print(f"No, I'm afraid the answer is {answer}.")

print(f"I asked you {numqs} questions. You got {correct} of them right.\n")

