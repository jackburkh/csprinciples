num_people = 5
amount_per_person = 4
price = 0.5
total = num_people * amount_per_person
num_wings =  total / price
print(f"You can order {total} wings when you have {num_people} people who can each spend {amount_per_person} dollars and wings cost {price} each.")

